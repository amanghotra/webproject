package test;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class day4 {

	@BeforeClass
	public void DeleteHomeLoanTable() {
		System.out.println("Delete Home Loan Table");
	}

	@Test(groups = { "Smoke" })
	public void WebloginHomeLoan() {
		System.out.println("webloginHome");
	}

	@Test(groups = { "Smoke" })
	public void MobileLoginHomeLoan() {
		System.out.println("MobileloginHome");
	}

	@Test(groups = { "Regression" })
	public void LoginAPIHomeLoan() {
		System.out.println("APIloginHome");
	}

}
